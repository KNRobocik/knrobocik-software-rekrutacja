from enum import Enum
import random


class Direction(Enum):
    LEFT = 1
    UP = 2
    RIGHT = 3
    DOWN = 4
    

class State(Enum):
    SUCCESS = 1
    LOSS = 2
    IN_PROGRESS = 3


def pop_random(arr):
    element = random.choice(arr)
    arr.remove(element)
    return element
