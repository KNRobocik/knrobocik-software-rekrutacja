from utils import *

class Game:
    def __init__(self, n, p) -> None:
        self.obstacles = []
        self.dest = (-1, -1)
        self.rov = ROV()
        
        self.n = n
        self.p = p
        self.reset()
        
    def reset(self):
        while not self.generate():
            pass
        
    @staticmethod
    def search(arr, start):
        n = len(arr)
        q = [start]
        visited = set()
        
        while q:
            x, y = q.pop(0)
            visited.add((x, y))
            if y + 1 < n and arr[y + 1][x] == ' ' and (x, y + 1) not in visited:
                q.append((x, y + 1))
            if y - 1 >= 0 and arr[y - 1][x] == ' ' and (x, y - 1) not in visited:
                q.append((x, y - 1))
            if x + 1 < n and arr[y][x + 1] == ' ' and (x + 1, y) not in visited:
                q.append((x + 1, y))
            if x - 1 >=0 and arr[y][x - 1] == ' ' and (x - 1, y) not in visited:
                q.append((x - 1, y))
                
        visited.remove(start)
                
        return list(visited)
    
    def generate(self):
        self.obstacles = []
        positions = []
        for i in range(self.n):
            for j in range(self.n):
                positions.append((i, j))
        
        for _ in range(self.p):
            self.obstacles.append(pop_random(positions))
            
        self.dest = pop_random(positions)
        self.rov.position = self.dest
        
        rov_positions = self.search(self.array, self.dest)
        if not rov_positions:
            return False
        
        self.rov.position = pop_random(rov_positions)
        return True

    @property
    def array(self):
        arr = [[' '] * self.n for _ in range(self.n)]
        for o_x, o_y in self.obstacles:
            arr[o_y][o_x] = 'o'
            
        r_x, r_y = self.rov.position
        arr[r_y][r_x] = 'r'
        
        d_x, d_y = self.dest
        arr[d_y][d_x] = 'x'
        return arr
    
    def check_state(self):
        if self.rov.position == self.dest:
            self.reset()
            return State.SUCCESS
        if (self.rov.position in self.obstacles 
            or self.rov.x not in range(0, self.n) 
            or self.rov.y not in range(0, self.n)):
            self.reset()
            return State.LOSS
        return State.IN_PROGRESS
    
    def update(self, action):
        self.rov.move(action)
        return self.check_state()
        
        
class ROV:
    def __init__(self) -> None:
        self.x = 0
        self.y = 0
        
    @property
    def position(self):
        return self.x, self.y
    
    @position.setter
    def position(self, new_pos):
        self.x, self.y = new_pos
    
    def move(self, direction):
        if direction == Direction.UP:
            self.y -= 1
        elif direction == Direction.DOWN:
            self.y += 1
        elif direction == Direction.LEFT:
            self.x -= 1
        elif direction == Direction.RIGHT:
            self.x += 1
       