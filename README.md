# Uruchamianie

```bash
python server.py
```

Powyższa komenda uruchomi serwer na localhoscie na porcie 8000. 

# API

W celu pobrania, bieżącego stanu mapy należy wysłać zapytanie GET na adres:

```bash
http://localhost:8000
```

Mapa zostanie zwrócona w formacie (poniższa mapa jest tylko przykładem):

```bash
{ "game_state": [
[" ", "o", " ", "o", " ", " ", " ", " ", "o", "o"],
["o", " ", " ", " ", "o", " ", "o", "o", "o", "o"],
[" ", "o", " ", "x", "o", " ", " ", "o", " ", " "],
["o", " ", "r", " ", " ", " ", " ", " ", " ", "o"],
[" ", " ", " ", "o", " ", " ", " ", " ", " ", " "],
[" ", "o", "o", " ", " ", " ", "o", " ", " ", " "],
[" ", " ", "o", " ", " ", " ", "o", "o", "o", " "],
[" ", "o", " ", " ", " ", " ", " ", " ", "o", " "],
["o", " ", "o", " ", " ", " ", "o", " ", " ", " "],
["o", "o", " ", " ", " ", " ", " ", " ", " ", " "]
]}
```

gdzie "r" - ROV, "o" - przeszkoda, "x" - cel,  " " - puste pole.

W celu wykonania akcji dronem należy wysłać zapytanie POST na ten sam adres.
W ciele (requestBody) zapytania należy zawrzeć dane w formacie JSON z kluczem "action", do którego przypisana będzie liczba całkowita w zakresie od 1 do 4. Wartości parametru action, odpowiadają następującym akcjom:

1 - ruch o jedno pole w lewo,
2 - ruch o jedno pole w górę,
3 - ruch o jedno pole w prawo,
4 - ruch o jedno pole w dół.

Przykładowe ciało zapytania POST:

```bash
{
	"action": 3
}
```

W odpowiedzi na zapytanie POST wysłany zostanie wynik działania w formacie:

```bash
{
	"result": 3
}
```

gdzie wartość parametru result odpowiada następującym wynikom:

1 - sukces, dron dopłynął do celu, 
2 - porażka, dron trafił w przeszkodę albo wypłynął poza mapę, 
3 - w toku, dron wpłynął na puste pole.

# Zasady działania symulatora

Jeśli w wyniku wykonania zapytania POST dron odniesie sukces lub porażkę, gra zostanie automatycznie zresetowana. Mapy generowane są w taki sposób, że zawsze istnieje droga od drona do miejsca dokowania.

# Reinforcement Learning

Dla osób, które zdecydowały się robić zadanie z reinforcement learningu został przygotwany prosty interfejs w klasie GameApi, który pozwala na komunikację z grą, bez korzystania z protokołu HTTP.

# Dodatkowe informacje

W razie jakichkolwiek problemów z uruchomieniem lub błędnym działaniem serwera prosimy konakt na email: knrobocik.rekrutacja@gmail.com
