from http.server import BaseHTTPRequestHandler, HTTPServer
import json

from game import Game
from utils import Direction

MAP_SIZE = 10
N_OBSTACLES = 30

def inject_game(cls):
    cls.game = Game(MAP_SIZE, N_OBSTACLES)  
    return cls   

@inject_game
class Server(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','application/json')
        self.end_headers()

        game_state = self.game.array
        response = {'game_state': game_state}
        response_json = json.dumps(response)

        self.wfile.write(bytes(response_json, "utf8"))
        
    def do_POST(self):
        content_len = int(self.headers.get('Content-Length'))
        post_body = self.rfile.read(content_len)
        body_dict = json.loads(post_body)
        action = body_dict.get("action")
        if not action:
            self.send_response(400)
            self.send_header('Content-type','application/json')
            self.end_headers()
            response = {'error': 'no action in request body'}
            response_json = json.dumps(response)
            self.wfile.write(bytes(response_json, "utf8"))
            return 
        
        if action == 1: direction = Direction.LEFT
        elif action == 2: direction = Direction.UP
        elif action == 3: direction = Direction.RIGHT
        elif action == 4: direction = Direction.DOWN
        else:
            self.send_response(400)
            self.send_header('Content-type','application/json')
            self.end_headers()
            response = {'error': 'incorrect action'}
            response_json = json.dumps(response)
            self.wfile.write(bytes(response_json, "utf8"))
            return 
        
        result = self.game.update(direction)
        response = {'result': result.value}
        response_json = json.dumps(response)
        
        self.send_response(200)
        self.send_header('Content-type','application/json')
        self.end_headers()
        self.wfile.write(bytes(response_json, "utf8"))


if __name__ == "__main__":
    with HTTPServer(('', 8000), Server) as server:
        server.serve_forever()
