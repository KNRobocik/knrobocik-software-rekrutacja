from server import inject_game
from utils import Direction

@inject_game
class GameApi():
    
    @staticmethod
    def get_map():
        return GameApi.game.array
    
    @staticmethod
    def make_action(action):
        if action == 1: direction = Direction.LEFT
        elif action == 2: direction = Direction.UP
        elif action == 3: direction = Direction.RIGHT
        elif action == 4: direction = Direction.DOWN
        return GameApi.game.update(direction).value
